<?php


namespace App\Controller;

use App\Monitoring\MonitoringCpu;
use App\Monitoring\MonitoringDisk;
use App\Monitoring\MonitoringHttpd;
use App\Monitoring\MonitoringRam;
use App\Repository\BlendRepository;
use App\Repository\TaskRepository;
use App\Service\HttpdConfigService;
use App\Tool\Size;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle routes for admins
 *
 * @Route("/admin")
 */
class AdminController extends AbstractController {

    private BlendRepository $blendRepository;
    private TaskRepository $taskRepository;
    private HttpdConfigService $httpdConfigService;

    public function __construct(
        BlendRepository $blendRepository,
        HttpdConfigService $httpdConfigService,
        TaskRepository $taskRepository) {

        $this->httpdConfigService = $httpdConfigService;
        $this->blendRepository = $blendRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @Route("/status")
     */
    public function status() : Response {
        $blends = $this->blendRepository->findAll();
        foreach ($blends as $k => $blend) {
            $blend->disk_usage = Size::humanSize($blend->getSize());
        }

        $monitoring = array();
        foreach (array(new MonitoringHttpd(), new MonitoringCpu(), new MonitoringRam(), new MonitoringDisk($this->blendRepository)) as $monitor) {
            $data = array('type' => $monitor->getType(), 'value' => $monitor->getHumanValue());

            $monitoring [] = $data;
        }
        $monitoring [] = array('type' => 'max-request', 'value' => $this->httpdConfigService->getSavedMaxRequest());

        return $this->render('status.html.twig', [
            'version' => getenv('VERSION'),
            'tasks' => $this->taskRepository->findAll(),
            'monitoring' => $monitoring,
            'blends' => $blends
        ]);
    }
}