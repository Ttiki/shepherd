<?php

namespace App\Controller;

use App\Entity\Blend;
use App\Entity\Tile;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request from project's owner
 * @Route("/blend")
 */
class OwnerController extends AbstractController {

    private BlendService $blendService;
    private BlendRepository $blendRepository;
    private FrameRepository $frameRepository;
    private TileRepository $tileRepository;

    public function __construct(
        BlendService $blendService,
        BlendRepository $blendRepository,
        FrameRepository $frameRepository,
        TileRepository $tileRepository) {

        $this->blendService = $blendService;
        $this->blendRepository = $blendRepository;
        $this->frameRepository = $frameRepository;
        $this->tileRepository = $tileRepository;
    }

    /**
     * @Route("/{token}/{blend}/frame/{frameNumber}/full", methods="GET")
     */
    public function getFrame(string $token, Blend $blend, int $frameNumber) : BinaryFileResponse {
        if ($this->blendService->isOwnerTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }
        $frame = $this->frameRepository->findOneBy(array('blend' => $blend->getId(), 'number' => $frameNumber));
        if (is_object($frame)) {
            $path = $this->frameRepository->getFullPath($frame);

            return $this->giveFile($path);
        }
        else {
            throw $this->createNotFoundException('file not found');
        }
    }

    /**
     * @Route("/{token}/{blend}/tile/{tile}/full", methods="GET")
     */
    public function getTile(string $token, Blend $blend, Tile $tile) : BinaryFileResponse {
        if ($this->blendService->isOwnerTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }
        $path = $this->tileRepository->getPath($tile);

        return $this->giveFile($path);
    }

    /**
     * @Route("/{token}/{blend}/mp4/final", methods="GET")
     */
    public function getMp4Final(string $token, Blend $blend) : BinaryFileResponse {
        if ($this->blendService->isOwnerTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }

        $path = $this->blendRepository->getMP4FinalPath($blend);

        return $this->giveFile($path);
    }

    /**
     * @Route("/{token}/{blend}/mp4/preview", methods="GET")
     */
    public function getMp4Preview(string $token, Blend $blend) : BinaryFileResponse {
        if ($this->blendService->isOwnerTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }

        $path = $this->blendRepository->getMP4PreviewPath($blend);

        return $this->giveFile($path);
    }

    /**
     * @Route("/{token}/{blend}/zip", methods="GET")
     */
    public function getZip(string $token, Blend $blend) : BinaryFileResponse {
        if ($this->blendService->isOwnerTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }

        $path = $this->blendRepository->getZip($blend);

        return $this->giveFile($path);
    }

    private function giveFile(string $path) {
        try {
            $file = new File($path, true);
        } catch (FileNotFoundException $e) {
            throw $this->createNotFoundException('file not found');
        }

        header('Content-Type: '.$file->getMimeType());
        header('Content-Length: '.filesize($file));

        return $this->file($file, $file->getFilename(), ResponseHeaderBag::DISPOSITION_INLINE);
    }
}