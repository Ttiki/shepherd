<?php


namespace App\Service;

use App\Entity\Task;
use Psr\Log\LoggerInterface;

class TaskService {

    private LoggerInterface $logger;
    private TileService $tileService;
    private FrameService $frameService;
    private BlendService $blendService;
    private MasterService $masterService;


    public function __construct(MasterService $masterService, BlendService $blendService, FrameService $frameService, TileService $tileService, LoggerInterface $logger) {
        $this->logger = $logger;
        $this->tileService = $tileService;
        $this->frameService = $frameService;
        $this->blendService = $blendService;
        $this->masterService = $masterService;
    }

    public function execute(Task $task) : bool {
        $this->logger->debug(__method__.' type: '.$task->getType());

        if ($task->getType() == Task::TYPE_GENERATE_FRAME_THUMBNAIL && is_object($task->getFrame())) {
            $this->logger->debug(__method__.' will : frameService->generateImageThumbnail');
            return $this->frameService->generateImageThumbnail($task->getFrame());
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_TILE_THUMBNAIL && is_object($task->getTile())) {
            $this->logger->debug(__method__.' will : tileService->generateThumbnail');
            return $this->tileService->generateThumbnail($task->getTile());
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_FRAME && is_object($task->getFrame())) {
            $this->logger->debug(__method__.' will : frameService->generateImage');
            return $this->frameService->generateImage($task->getFrame());
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_MP4_FINAL && is_object($task->getBlend())) {
            $this->logger->debug(__method__.' will : blendService->generateMP4Final');
            if ($this->blendService->generateMP4Final($task->getBlend())) {
                return $this->masterService->notifyGeneratedMP4Final($task->getBlend());
            }
            else {
                return $this->masterService->notifyGeneratedMP4FinalFailed($task->getBlend());
            }
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_MP4_PREVIEW && is_object($task->getBlend())) {
            $this->logger->debug(__method__.' will : blendService->generateMP4Preview');
            if ($this->blendService->generateMP4Preview($task->getBlend())) {
                return $this->masterService->notifyGeneratedMP4Preview($task->getBlend());
            }
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_ZIP && is_object($task->getBlend())) {
            $this->logger->debug(__method__.' will : blendService->generateZIP');
            if ($this->blendService->generateZIP($task->getBlend())) {
                return $this->masterService->notifyGeneratedZIP($task->getBlend());
            }
            else {
                return $this->masterService->notifyGeneratedZIPFailed($task->getBlend());
            }
        }
        elseif ($task->getType() == Task::TYPE_DELETE_BLEND && is_object($task->getBlend())) {
            $this->logger->debug(__method__.' will : blendService->delBlend');
            return $this->blendService->delBlend($task->getBlend());
        }

        $this->logger->error(__method__.' failed to execute task, unknown type '.$task->getType());
        return false;
    }
}