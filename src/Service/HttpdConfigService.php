<?php


namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class HttpdConfigService {

    private ContainerBagInterface $containerBag;

    public function __construct(ContainerBagInterface $containerBag) {
        $this->containerBag = $containerBag;
    }

    /**
     * Get max request value from the config
     */
    public function getMaxRequest() : int {
        if ($this->containerBag->get('httpd_config') == 'AUTODETECT') {
            foreach (explode("\n", file_get_contents('/proc/meminfo')) as $line) {
                if (substr($line, 0, strlen("MemAvailable:")) == "MemAvailable:") {
                    $ramkB = (int)trim(str_replace("kB", '', substr($line, strlen("MemAvailable:"))));
                    return ($ramkB - 1000000) * 1000 / 80000000; // 1 GB of safety net, in average fpm take 80MB of ram per request
                }
            }

            return 250;
        }
        else {
            return (int)$this->containerBag->get('httpd_max_request');
        }
    }

    /**
     * Get the saved value (in /etc/apache2/mods-available/mpm_event.conf) of max request
     */
    public function getSavedMaxRequest() : int {
        foreach (explode("\n", file_get_contents("/etc/apache2/mods-available/mpm_prefork.conf")) as $line) {
            if ($pos = strpos($line, "MaxRequestWorkers")) {
                $val = trim(substr($line, $pos + strlen("MaxRequestWorkers")));
                if (is_numeric($val)) {
                    return (int)$val;
                }
            }
        }
        return -1;
    }
}